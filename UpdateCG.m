function [CG,edgeL,Adj,X]=UpdateCG(CG,f,s,Adj,edgeL,X)
d=size(Adj,1);
index=0;
for i=1:1:f-1
    index=index+(d-i);
end
index=index+(s-f);
minSep=find(ismember(edgeL(index).MS,1));

X=X+1;
edgeL(index).eliOrder=X;
edgeL(index).status=0;

Adj(f,s)=1;
Adj(s,f)=1;

r=size(CG.Component,1);
CG.Component(r+1,f)=1;
CG.Component(r+1,s)=1;
if isempty(minSep) == 0
    CG.Component(r+1,minSep)=1;
end
CG.Clique(r+1,r+1)=0;

pos=zeros(r,1);
for i=1:1:r
    if sum(bitand(CG.Component(i,:),CG.Component(r+1,:)) == CG.Component(i,:)) == d
        pos(i)=1;
    end
end
pos=find(ismember(pos,1));
CG.Component(pos,:)=[];     % Removing the merged cliques from the list.
CG.Clique(pos,:)=[];
CG.Clique(:,pos)=[];

r=size(CG.Component,1);
for i=1:1:r-1
    for j=i+1:1:r
        if sum(bitand(CG.Component(i,:),CG.Component(j,:))) ~= 0
            [minSep,edgeL]=minSepCliques(CG.Component(i,:),CG.Component(j,:),Adj,edgeL,d);
            if minSep ~= -1
                CG.Clique(i,j)=1;
                CG.Clique(j,i)=1;
            else
                CG.Clique(i,j)=0;
                CG.Clique(j,i)=0;
            end
        else
            CG.Clique(i,j)=0;
            CG.Clique(j,i)=0;
        end
    end
end
CG.CComp

Cindexf=find(ismember(CG.CComp(:,f),1));
Cindexs=find(ismember(CG.CComp(:,s),1));
if Cindexf ~= Cindexs
    Compf=CG.CComp(Cindexf,:);
    Comps=CG.CComp(Cindexs,:);
    Compf(f)=0;   % Removing the f node from its connected component list
    Comps(s)=0;   % Removing the s node from its connected component list
    Compf=find(ismember(Compf,1));
    Comps=find(ismember(Comps,1));
    for i=1:1:size(Compf,2)
        for j=1:1:size(Comps,2)
            if Compf(i) < Comps(j)
                index=FindIndexEdge(Compf(i),Comps(j),d);
            else
                index=FindIndexEdge(Comps(j),Compf(i),d);
            end
            if edgeL(index).status ~= 0
                edgeL(index).status=2;
            end
        end
    end
    
    Nf=find(ismember(Adj(f,:),1));
    if isempty(Nf) == 0
        Compf=CG.CComp(Cindexf,:);
        Compf(Nf)=0;
        Compf(f)=0;
        Compf=find(ismember(Compf,1));
        for i=1:1:size(Compf,2) 
            if Compf(i) < s
                index=FindIndexEdge(Compf(i),s,d);
            else
                index=FindIndexEdge(s,Compf(i),d);
            end
            if edgeL(index).status ~= 0
                edgeL(index).status=2;
            end
        end
    end
    
    Ns=find(ismember(Adj(s,:),1));
    if isempty(Ns) == 0
        Comps=CG.CComp(Cindexs,:);
        Comps(Ns)=0;
        Comps(s)=0;
        Comps=find(ismember(Comps,1));
        for i=1:1:size(Comps,2) 
            if Comps(i) < f
                index=FindIndexEdge(Comps(i),f,d);
            else
                index=FindIndexEdge(f,Comps(i),d);
            end
            if edgeL(index).status ~= 0
                edgeL(index).status=2;
            end
        end
    end
else
    newC=size(CG.Component,1);
    connectL=zeros(1,newC);
    for i=1:1:newC-1
        if CG.Clique(i,newC) == 1
            connectL(i)=1;
        end
    end
    connectL=find(ismember(connectL,1));
    for i=1:1:size(connectL,2)-1
        for j=i+1:1:size(connectL,2)
            if CG.Clique(connectL(i),connectL(j)) == 0
                CG1=CG.Component(connectL(i),:);
                CG2=CG.Component(connectL(j),:);
                common=find(ismember(bitand(CG1,CG2),1));
                CG1(common)=0;
                CG2(common)=0;
                CG1=find(ismember(CG1,1));
                CG2=find(ismember(CG2,1));
                for k=1:1:size(CG1,2)
                    for l=1:1:size(CG2,2)
                        if CG1(k) < CG2(l)
                            index=FindIndexEdge(CG1(k),CG2(l),d);
                        else
                            index=FindIndexEdge(CG2(l),CG1(k),d);
                        end
                        if edgeL(index).status ~= 0
                            edgeL(index).status=2;
                        end
                    end
                end
            end
        end
    end
end




tempComp=zeros(1,size(CG.Clique,2)); % Its a vector to get the information which cliques forms a connected components
tempComp(1)=1;
tempId=1;
for i=1:1:size(CG.Clique,2)
    nClique=find(ismember(CG.Clique(i,:),1));
    if tempComp(i) == 0
        x=find(tempComp(nClique));
        if isempty(x) ~= 1
            if size(x,2) == 1
                tempComp(i)=tempComp(nClique(x))
            else
                tempComp(i)=tempComp(nClique(x(1)))
            end
        else
            tempId=tempId+1;
            tempComp(i)=tempId;
        end
    end
    tempComp(nClique)=tempComp(i);
end

sizeC=size(CG.CComp,1);
for i=1:1:tempId
    x=find(ismember(tempComp,i));
    y=CG.Component(x(1),:);
    for j=1:1:size(x,2)-1
        y=bitor(y,bitor(CG.Component(x(j),:),CG.Component(x(j+1),:)));
    end
    CG.CComp(i,:)=y;
end
CG.CComp(tempId+1:sizeC,:)=[];


Nf=find(ismember(Adj(f,:),1));
for i=1:1:size(Nf,2)
    if s < Nf(i) && s ~= Nf(i)
        index=FindIndexEdge(s,Nf(i),d);
        if edgeL(index).status ~= 0
            edgeL(index).status=1;
        end
    elseif s > Nf(i) && s ~= Nf(i)
        index=FindIndexEdge(Nf(i),s,d);
        if edgeL(index).status ~= 0
            edgeL(index).status=1;
        end
    end
    
end

Ns=find(ismember(Adj(s,:),1));
for i=1:1:size(Ns,2)
    if f < Ns(i) && f ~= Ns(i)
        index=FindIndexEdge(f,Ns(i),d);
        if edgeL(index).status ~= 0
            edgeL(index).status=1;
        end
    elseif f > Ns(i) && f ~= Ns(i)
        index=FindIndexEdge(Ns(i),f,d);
        if edgeL(index).status ~= 0
            edgeL(index).status=1;
        end
    end
    
end