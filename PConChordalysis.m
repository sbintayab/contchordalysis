function G=PConChordalysis(file,option,param)

Total=0;
Data=xlsread(file);
fprintf(' Data loading is complete');

[n,d]=size(Data);

CG.Clique=zeros(d,d);       
CG.Component=zeros(d,d);    
for i=1:1:d
    CG.Component(i,i)=1; 
end


nedge=d*(d-1)/2;
pos=1;
for i=1:1:d-1
    for j=i+1:1:d
        edgeL(pos).fn=i;
        edgeL(pos).sn=j;
        edgeL(pos).MS=zeros(1,d); 
        edgeL(pos).status=1;      
        edgeL(pos).eliOrder=0;    
        pos=pos+1;
    end
end
X=0;


Adj=zeros(d,d);


PQ=ones(1,nedge).*1000;


for i=1:1:nedge
    edge=edgeL(i);
    if option == 1
        PQ(i)=MMLparam(edge,Data);
    elseif option == 4 || option == 6
        PQ(i)=logLikelihood(edge,Data);
    elseif option == 5
        PQ(i)=MDL(edge,Data);
    else
        PQ(i)=SSDpValue(edge,Data);
    end
end
step=0;
MMLR=0;
done=1;
old=0;
curr=0;


while done == 1
    fprintf('\n\n ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^');
    if option == 1 || option == 2 || option == 3
        minMML(1)=min(PQ);                          
        minMML(2)=find(ismember(PQ,minMML(1)),1);   
        PQ(minMML(2))=1000000;                      

    elseif option == 4 || option == 5 || option == 6
        minMML(1)=max(PQ);
        minMML(2)=find(ismember(PQ,minMML(1)),1);   
        PQ(minMML(2))=-1000000;                     

    end
    if option == 1 
        MMLG = log((nedge-step)/(nedge-step-1));
        MMLC = MMLG+minMML(1);  
        if MMLC < MMLR
            [PQ,CG,edgeL,Adj,X]=PQandGraphAdjust(PQ,edgeL,Adj,minMML(2),CG,Data,X,option);
        else
            done=0;
        end
        
    elseif option == 4
        curr=(2*minMML(1))+log(n);
        if curr >= old
            [PQ,CG,edgeL,Adj,X]=PQandGraphAdjust(PQ,edgeL,Adj,minMML(2),CG,Data,X,option);
        else
            done=0;
        end
        old=curr;
        
    elseif option == 5
        curr=minMML(1);
        if curr <= old
            [PQ,CG,edgeL,Adj,X]=PQandGraphAdjust(PQ,edgeL,Adj,minMML(2),CG,Data,X,option);
        else
            done=0;
        end
        old=curr;
    elseif option == 6
        curr=minMML(1)+2;
        if curr >= old
            [PQ,CG,edgeL,Adj,X]=PQandGraphAdjust(PQ,edgeL,Adj,minMML(2),CG,Data,X,option);
        else
            done=0;
        end
        old=curr;
    
    elseif option == 3
        curr=param.eta*(param.alpha-oldtau);
        if curr >= minMML(1)
            [PQ,CG,edgeL,Adj,X]=PQandGraphAdjust(PQ,edgeL,Adj,minMML(2),CG,Data,X,option);
        else
            done=0;
        end
        old=old+curr;
      
    else
        curr=(param.alpha/(2^step));
        if minMML(1) <= curr
            [PQ,CG,edgeL,Adj,X]=PQandGraphAdjust(PQ,edgeL,Adj,minMML(2),CG,Data,X,option);
        else
            done=0;
        end
    end

    if done ~= 0
        PQContent=find(PQ==1000000);
        PQtemp=PQ;
        PQtemp(PQContent)=[];
    end
    
    
    step=step+1;
    if done ~= 0 && (step > nedge || isempty(PQtemp) == 1)
        done=0;
    end
end
G=Adj;