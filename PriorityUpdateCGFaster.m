function [CG,edgeL,Adj,X,PQ]=PriorityUpdateCGFaster(CG,f,s,Adj,edgeL,X,PQ,Data,option)
sameComp=0;
Compf=FindComponent(Adj,f);
if Compf(s) == 1
    Comps=Compf;
    sameComp=1;
else
    Comps=FindComponent(Adj,s);
end
clear x

d=size(Adj,1);
index=0;
for i=1:1:f-1
    index=index+(d-i);
end
index=index+(s-f);
minSep=find(ismember(edgeL(index).MS,1));

X=X+1;
edgeL(index).eliOrder=X;
edgeL(index).status=0;

Adj(f,s)=1;
Adj(s,f)=1;

r=size(CG.Component,1);
CG.Component(r+1,f)=1;
CG.Component(r+1,s)=1;
if isempty(minSep) == 0
    CG.Component(r+1,minSep)=1;
end
CG.Clique(r+1,r+1)=0;
clear minSep

newPos=size(CG.Component,1);

Comp = bitor(Compf,Comps);
CompClique=CliquesInSameComp(Comp,CG.Clique,CG.Component);

fprintf('\n Removing the small cliques which are mergable with new clique')
CompClique(newPos)=0;
smCompClique=find(ismember(CompClique,1));
pos=zeros(r,1);
if isempty(smCompClique) == 0
	numCl=size(smCompClique,2);             % Their number also
    for i=1:1:numCl
        x=bitand(CG.Component(smCompClique(i),:),CG.Component(newPos,:));
        y=CG.Component(smCompClique(i),:);
        if x == y
            pos(smCompClique(i))=1;
        end
    end
    pos=find(ismember(pos,1));
    if isempty(pos) == 0
        x=sprintf('%d ',pos);
        fprintf('\n value of pos : %s',x);
        CG.Component(pos,:)=[];     % Removing the merged cliques from the list.
        CG.Clique(pos,:)=[];
        CG.Clique(:,pos)=[];
        CompClique(pos)=[];
    end
end

newPos=size(CG.Component,1);
smCompClique=find(ismember(CompClique,1));   % Finding which cliques are in same components
if isempty(smCompClique) == 0
	numCl=size(smCompClique,2);              % Their number also
    for i=1:1:numCl
        if sum(bitand(CG.Component(smCompClique(i),:),CG.Component(newPos,:))) ~= 0
			[minSep,edgeL,PQ]=minSepCliques(CG.Component(smCompClique(i),:),CG.Component(newPos,:),Adj,edgeL,Data,PQ,option);
			if minSep ~= -1
                CG.Clique(smCompClique(i),newPos)=1;
				CG.Clique(newPos,smCompClique(i))=1;
			else
				CG.Clique(smCompClique(i),newPos)=0;
				CG.Clique(newPos,smCompClique(i))=0;
            end
        else
            [PQ,edgeL] = EdgeDisable(PQ,edgeL,CG,smCompClique(i),newPos,option);	
			CG.Clique(smCompClique(i),newPos)=0;
			CG.Clique(newPos,smCompClique(i))=0;
        end
    end
end

clear newPos numCl smCompClique CompClique Comp

if sameComp == 0 
    Compf(f)=0;
    Comps(s)=0;
    eleF=find(ismember(Compf,1));
    eleS=find(ismember(Comps,1));
    if isempty(eleF) == 0 && isempty(eleS) == 0
        for i=1:1:size(eleF,2)
            for k=1:1:size(eleS,2)
                if eleF(i) < eleS(k)
                    index=FindIndexEdge(eleF(i),eleS(k),d);
                else
                    index=FindIndexEdge(eleS(k),eleF(i),d);
                end
                edgeL(index).status=2;
                if option == 1 || option == 2 || option == 3
                    PQ(index)=1000000;
                else
                    PQ(index)=-1000000;
                end
            end
        end
    end
end
clear Compf Comps eleF eleS index i k