function [minSep,edgeL,PQ]=minSepCliques(CGn,CG1,Adj,edgeL,Data,PQ,option)
%% Size of the data
d=size(Adj,2);

%% Temporary CGn and CG1
tCGn=CGn;
tCG1=CG1;
% x=sprintf('%d ',find(ismember(CGn,1)));
% fprintf('\n ##### Minimal separator find of CGn : %s',x);
% x=sprintf('%d ',find(ismember(CG1,1)));
% fprintf('\n ##### Minimal separator find of CG1 : %s',x);

%% Finding the uncommon elements of CGn and CG1 cliques
common=bitand(CGn,CG1);
eleCom=find(ismember(common,1));
tCGn(eleCom)=0; % tCGn contains uncommon elements 
tCG1(eleCom)=0; % tCG1 contains uncommon elements

%% unCommon Elements of CGn and CG1
eleCGn=find(ismember(tCGn,1));
eleCG1=find(ismember(tCG1,1));
% x=sprintf('%d ',eleCGn);
% fprintf('\n ##### Minimal separator find of CGn without common : %s',x);
% x=sprintf('%d ',eleCG1);
% fprintf('\n ##### Minimal separator find of CG1 without common: %s',x);

%%
minSep=eleCom;
if size(eleCGn,2) == 1 && size(eleCG1,2) == 1
    minSep=eleCom;
    if eleCGn < eleCG1
        edgeI=FindIndexEdge(eleCGn,eleCG1,d);
    else
        edgeI=FindIndexEdge(eleCG1,eleCGn,d);
    end
    if edgeL(edgeI).status ~= 0
        %fprintf('\n ##### (1) Edge re-examine: (%d,%d)',edgeL(edgeI).fn,edgeL(edgeI).sn);
        edgeL(edgeI).MS(minSep)=1;
        if option == 1
            PQ(edgeI)=MMLparam(edgeL(edgeI),Data);
        elseif option == 4 || option == 6
            PQ(edgeI)=logLikelihood(edgeL(edgeI),Data);
        elseif option == 5
            PQ(edgeI)=MDL(edgeL(edgeI),Data);
        else
            PQ(edgeI)=SSDpValue(edgeL(edgeI),Data);
        end
    end
        
elseif size(eleCGn,2) == 1 && size(eleCG1,2) ~= 1
    for i=1:1:size(eleCG1,2)-1
        if isequal(bitand(Adj(eleCGn,:),Adj(eleCG1(i),:)),bitand(Adj(eleCGn,:),Adj(eleCG1(i+1),:))) ~= 1
            minSep=-1;
            break
        end
    end
    for i=1:1:size(eleCG1,2)
        if eleCGn < eleCG1(i)
            edgeI=FindIndexEdge(eleCGn,eleCG1(i),d);
        else
            edgeI=FindIndexEdge(eleCG1(i),eleCGn,d);
        end
        if edgeL(edgeI).status ~= 0
            if minSep ~= -1
                %fprintf('\n ##### (2) Edge re-examine: (%d,%d)',edgeL(edgeI).fn,edgeL(edgeI).sn);
                edgeL(edgeI).MS(minSep)=1;
                if option == 1
                    PQ(edgeI)=MMLparam(edgeL(edgeI),Data);
                elseif option == 4 || option == 6
                    PQ(edgeI)=logLikelihood(edgeL(edgeI),Data);
                elseif option == 5
                    PQ(edgeI)=MDL(edgeL(edgeI),Data);
                else
                    PQ(edgeI)=SSDpValue(edgeL(edgeI),Data);
                end
            else
                %fprintf('\n ***** (1) Edge disable : (%d,%d)',edgeL(edgeI).fn,edgeL(edgeI).sn);
                edgeL(edgeI).status=2;
                if option == 1 || option == 2 || option == 3
                    PQ(edgeI)=1000000;
                else
                    PQ(edgeI)=-1000000;
                end
            end
        end
    end
    
elseif size(eleCGn,2) ~= 1 && size(eleCG1,2) == 1
    for i=1:1:size(eleCGn,2)-1
        if isequal(bitand(Adj(eleCG1,:),Adj(eleCGn(i),:)),bitand(Adj(eleCG1,:),Adj(eleCGn(i+1),:))) ~= 1
            minSep=-1;
            break
        end
    end
    for i=1:1:size(eleCGn,2)
        if eleCGn(i) < eleCG1
            edgeI=FindIndexEdge(eleCGn(i),eleCG1,d);
        else
            edgeI=FindIndexEdge(eleCG1,eleCGn(i),d);
        end
        if edgeL(edgeI).status ~= 0
            if minSep ~= -1
                %fprintf('\n ##### (3) Edge re-examine: (%d,%d)',edgeL(edgeI).fn,edgeL(edgeI).sn);
                edgeL(edgeI).MS(minSep)=1;
                if option == 1
                    PQ(edgeI)=MMLparam(edgeL(edgeI),Data);
                elseif option == 4 || option == 6
                    PQ(edgeI)=logLikelihood(edgeL(edgeI),Data);
                elseif option == 5
                    PQ(edgeI)=MDL(edgeL(edgeI),Data);    
                else
                    PQ(edgeI)=SSDpValue(edgeL(edgeI),Data);
                end
            else
                %fprintf('\n ***** (2) Edge disable : (%d,%d)',edgeL(edgeI).fn,edgeL(edgeI).sn);
                edgeL(edgeI).status=2;
                if option == 1 || option == 2 || option == 3
                    PQ(edgeI)=1000000;
                else
                    PQ(edgeI)=-1000000;
                end
            end
        end
    end
else
    z=bitand(Adj(eleCG1(1),:),Adj(eleCGn(1),:));
    for i=1:1:size(eleCGn,2)
        for j=1:1:size(eleCG1,2)
            x=bitand(Adj(eleCG1(j),:),Adj(eleCGn(i),:));
            if isequal(z,x) ~= 1
                minSep=-1;
                break
            end
        end
        if minSep==-1
            break
        end
    end
    for i=1:1:size(eleCGn,2)
        for j=1:1:size(eleCG1,2)
            if eleCGn(i) < eleCG1(j)
                edgeI=FindIndexEdge(eleCGn(i),eleCG1(j),d);
            else
                edgeI=FindIndexEdge(eleCG1(j),eleCGn(i),d);
            end
            
            if edgeL(edgeI).status ~= 0
                if minSep ~= -1
                    %fprintf('\n ##### (4) Edge re-examine: (%d,%d)',edgeL(edgeI).fn,edgeL(edgeI).sn);
                    edgeL(edgeI).MS(minSep)=1;
                    if option == 1
                        PQ(edgeI)=MMLparam(edgeL(edgeI),Data);
                    elseif option == 4 || option == 6
                        PQ(edgeI)=logLikelihood(edgeL(edgeI),Data);
                    elseif option == 5
                        PQ(edgeI)=MDL(edgeL(edgeI),Data);
                    else
                        PQ(edgeI)=SSDpValue(edgeL(edgeI),Data);
                    end
                else
                    %fprintf('\n ***** (3) Edge disable : (%d,%d)',edgeL(edgeI).fn,edgeL(edgeI).sn);
                    edgeL(edgeI).status=2;
                    if option == 1 || option == 2 || option == 3
                        PQ(edgeI)=1000000;
                    else
                        PQ(edgeI)=-1000000;
                    end
                end
            end
        end
    end
%     for i=1:1:size(CG,1)
%         if sum(intersect(CGn,CG(i,:))) == sum(intersect(CGn,CG(i,:)))
%             minSep=-1;
%             break
%         end
%     end
end

clear tCGn tCG1 eleCom eleCGn eleCG1 common edgeI z x i j