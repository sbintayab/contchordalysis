function ComponentPrint(CG)
for i=1:1:size(CG.Component,1)
    x=sprintf('%d ',find(ismember(CG.Component(i,:),1)));
    fprintf('\n Clique %d : %s',i,x);
end

x=sprintf('%d ',CG.ConnectClique);
fprintf('\n Connected cliques and their component id: %s',x);

for i=1:1:size(CG.Clique,1)
    x=sprintf('%d ',find(ismember(CG.Clique(i,:),1)));
    fprintf('\n Clique %d is connected with cliques %s',i,x)
end