function [PQ,edgeL] = EdgeDisable(PQ,edgeL,CG,fst,scd,option)
CGf=CG.Component(fst,:);
CGs=CG.Component(scd,:);

%% Number of variables in Data
d=size(CG.Component,2);

%% Removing common vertices betwenn CGf and CGs
common=find(ismember(bitand(CGf,CGs),1));
CGf(common)=0;
CGs(common)=0;

%% Elements of CGf and CGs
eleCGf=find(ismember(CGf,1));
eleCGs=find(ismember(CGs,1));

%% Disabling the edges
for i=1:1:size(eleCGf,2)
    for j=1:1:size(eleCGs,2)
        if eleCGf(i) < eleCGs(j)
            index = FindIndexEdge(eleCGf(i),eleCGs(j),d);
        else
            index = FindIndexEdge(eleCGs(j),eleCGf(i),d);
        end
        
        if edgeL(index).status == 1
            %fprintf('\n ***** (4) Edge disable : (%d,%d)',edgeL(index).fn,edgeL(index).sn);
            edgeL(index).status = 2;
            if option == 1 || option == 2 || option == 3
                PQ(index)=1000000;
            else
                PQ(index)=-1000000;
            end
        end
    end
end