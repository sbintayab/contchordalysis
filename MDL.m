%% Computing the MDL for encoding parameters and data for an edge (fn,sn)
function M = MDL(edge,Data)
%% Dimension of data
n=size(Data,1);

%% separating the nodes from the edge variable
fn=edge.fn;
sn=edge.sn;

%% Initializing the MDL score
M=0;

%% Preparing S
if sum(edge.MS) == 0
    S=-1;
else
    S=find(ismember(edge.MS,1));
end

%% the number of samples n and the genes d
n = size(Data,1);

%% Preparing S, SUa, SUb and SUab
if S ~= -1
    X=sort([fn S]);     % SUa
    Y=sort([S sn]);     % SUb
    Z=sort([fn S sn]);  % SUab
else
    X=fn;
    Y=sn;
    Z=sort([fn sn]);
end

%% Mean calculation
muX=mean(Data(:,X));
muY=mean(Data(:,Y));
muZ=mean(Data(:,Z));
if S ~= -1
    muS=mean(Data(:,S));
else
    muS=0;
end

%% Covariance matrix calculation
covX=cov(Data(:,X));
covY=cov(Data(:,Y));
covZ=cov(Data(:,Z));
if S ~= -1
    covS=cov(Data(:,S));
else
    covS=0;
end

%% LL calculation for X
nodeX=size(X,2);
edgeX=nodeX*(nodeX-1)/2;
BX = (sum(logmvnpdf(Data(:,X),muX,covX))/n)+(edgeX*log(n))+nodeX+log(n);
clear X muX covX nodeX edgeX

%% LL calculation for Y
nodeY=size(Y,2);
edgeY=nodeY*(nodeY-1)/2;
BY = (sum(logmvnpdf(Data(:,Y),muY,covY))/n)+(edgeY*log(n))+nodeY+log(n);
clear Y muY covY nodeY edgeY

%% LL calculation for Z
nodeZ=size(Z,2);
edgeZ=nodeZ*(nodeZ-1)/2;
BZ = (sum(logmvnpdf(Data(:,Z),muZ,covZ))/n)+(edgeZ*log(n))+nodeZ+log(n);
clear Z muZ covZ nodeZ edgeZ
%% LL calculation for S
BS=0;
if S ~= -1
    nodeS=size(S,2);
    edgeS=nodeS*(nodeS-1)/2;
    BS = (sum(logmvnpdf(Data(:,S),muS,covS))/n)+(edgeS*log(n))+nodeS+log(n);
    clear nodeX edgeX
end
clear S muS covS

%% Computing the final MDL for adding an edge (fn,sn)
M=(BX+BY-BZ-BS);
clear BZ BS BX BY