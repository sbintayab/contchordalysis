function CompClique=CliquesInSameComp(Comp,Clique,Component)

CompClique=zeros(1,size(Clique,2));
for i=1:1:size(Clique,2)
    if sum(bitand(Comp,Component(i,:))) ~= 0
        CompClique(i)=1;
    end
end
