function graphOutput(fname,Adj)
N=size(Adj,2);

fid=fopen(fname,'w');

for i=1:1:N
    for j=1:1:N
        fprintf(fid,'%d ',Adj(i,j));
    end
    fprintf(fid,'\n');
end

fclose(fid);