%% Computing p-value of an edge
function P = SSDpValue(edge,Data)
%% separating the nodes from the edge variable
fn=edge.fn;
sn=edge.sn;

%% Preparing S
if sum(edge.MS) == 0
    S=-1;
else
    S=find(ismember(edge.MS,1));
end

%% the number of samples n and the genes d
[n,d] = size(Data);

%% Preparing S, SUa, SUb and SUab
if S ~= -1
    X=sort([fn S]);     % SUa
    Y=sort([S sn]);     % SUb
    Z=sort([fn S sn]);  % SUab
else
    X=fn;
    Y=sn;
    Z=sort([fn sn]);
end

%% Mean calculation
muX=mean(Data(:,X));
muY=mean(Data(:,Y));
muZ=mean(Data(:,Z));
if S ~= -1
    muS=mean(Data(:,S));
else
    muS=0;
end

%% P-value calculation for X
PX=sum(sum(bsxfun(@minus,Data(:,X),muX).^2));
clear X muX

%% P-value calculation for X
PY=sum(sum(bsxfun(@minus,Data(:,Y),muY).^2));
clear Y muY

%% P-value calculation for X
PZ=sum(sum(bsxfun(@minus,Data(:,Z),muZ).^2));
clear Z muZ

%% P-value calculation for S
if S ~= -1
    PS=sum(sum(bsxfun(@minus,Data(:,S),muS).^2));
    clear S muS
else
    PS=0;
end

%% Computing the deviations
dev=(PS*PZ)/(PX*PY);
clear PX PY PZ PS

%% Computing p-value
P=betacdf(dev,abs((n-d)/2),0.5);
