function [PQ,CG,edgeL,Adj,X]=PQandGraphAdjust(PQ,edgeL,Adj,eindex,CG,Data,X,option)
f=edgeL(eindex).fn;
s=edgeL(eindex).sn;
[CG,edgeL,Adj,X,PQ]=PriorityUpdateCGFaster(CG,f,s,Adj,edgeL,X,PQ,Data,option);


