%% Finding the complete node list of te component of f node
function tempComp = FindComponent(Adj,curr)
%% 
tempComp=zeros(1,size(Adj,2));     % Its a vector to get the information which cliques forms a connected components
tempComp(curr)=1;
tempComp = BFSComponent(tempComp,curr,Adj);
%tempComp=find(ismember(tempComp,1));