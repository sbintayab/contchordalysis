%% Computing the BIC for encoding parameters and data for an edge (fn,sn)
function B = BIC(edge,Data)
%% separating the nodes from the edge variable
fn=edge.fn;
sn=edge.sn;

%% Initializing the BIC score
B=0;

%% Preparing S
if sum(edge.MS) == 0
    S=-1;
else
    S=find(ismember(edge.MS,1));
end

%% the number of samples n and the genes d
n = size(Data,1);

%% Preparing S, SUa, SUb and SUab
if S ~= -1
    X=sort([fn S]);     % SUa
    Y=sort([S sn]);     % SUb
    Z=sort([fn S sn]);  % SUab
else
    X=fn;
    Y=sn;
    Z=sort([fn sn]);
end

%% Mean calculation
muX=mean(Data(:,X));
muY=mean(Data(:,Y));
muZ=mean(Data(:,Z));
if S ~= -1
    muS=mean(Data(:,S));
else
    muS=0;
end

%% Covariance matrix calculation
covX=cov(Data(:,X));
covY=cov(Data(:,Y));
covZ=cov(Data(:,Z));
if S ~= -1
    covS=cov(Data(:,S));
else
    covS=0;
end

%% LL calculation for X
BX = sum(logmvnpdf(Data(:,X),muX,covX));
clear X muX covX

%% LL calculation for Y
BY = sum(logmvnpdf(Data(:,Y),muY,covY));
clear Y muY covY

%% LL calculation for Z
BZ = sum(logmvnpdf(Data(:,Z),muZ,covZ));
clear Z muZ covZ

%% LL calculation for S
BS=0;
if S ~= -1
    BS = sum(logmvnpdf(Data(:,S),muS,covS));
end
clear S muS covS

%% Computing the final BIC for adding an edge (fn,sn)
B=-2*(BX+BY-BZ-BS);
clear BZ BS BX BY