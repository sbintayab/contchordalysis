function tempComp = BFSComponent(tempComp,curr,Adj)
%% Finding the neighbour of curr node in Clique graph
Nc=find(ismember(Adj(curr,:),1));

%%  Updating each of neighbour by assigning same component id
for i=1:1:size(Nc,2)
    if tempComp(Nc(i)) == 0
        tempComp(Nc(i)) = tempComp(curr);
        tempComp = BFSComponent(tempComp,Nc(i),Adj);
    end
end