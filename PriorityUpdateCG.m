function [CG,edgeL,Adj,X,PQ]=PriorityUpdateCG(CG,f,s,Adj,edgeL,X,PQ,Data,option)
d=size(Adj,1);
index=0;
for i=1:1:f-1
    index=index+(d-i);
end
index=index+(s-f);
minSep=find(ismember(edgeL(index).MS,1));

X=X+1;
edgeL(index).eliOrder=X;
edgeL(index).status=0;

Adj(f,s)=1;
Adj(s,f)=1;

r=size(CG.Component,1);
CG.Component(r+1,f)=1;
CG.Component(r+1,s)=1;
if isempty(minSep) == 0
    CG.Component(r+1,minSep)=1;
end
CG.Clique(r+1,r+1)=0;

pos=zeros(r,1);
for i=1:1:r
    if bitand(CG.Component(i,:),CG.Component(r+1,:)) == CG.Component(i,:)
        pos(i)=1;
    end
end
pos=find(ismember(pos,1));
CG.Component(pos,:)=[];     % Removing the merged cliques from the list.
CG.Clique(pos,:)=[];
CG.Clique(:,pos)=[];

Comp = FindComponent(Adj,f);
CompClique=CliquesInSameComp(Comp,CG.Clique,CG.Component);

r=size(CG.Component,1);
for i=1:1:r-1
    for j=i+1:1:r
        if sum(bitand(CG.Component(i,:),CG.Component(j,:))) ~= 0

            [minSep,edgeL,PQ]=minSepCliques(CG.Component(i,:),CG.Component(j,:),Adj,edgeL,Data,PQ,option);
            if minSep ~= -1
                CG.Clique(i,j)=1;
                CG.Clique(j,i)=1;
            else
                CG.Clique(i,j)=0;
                CG.Clique(j,i)=0;
            end
        else
            if CompClique(i) == 1 && CompClique(j) == 1
                [PQ,edgeL] = EdgeDisable(PQ,edgeL,CG,i,j);
                CG.Clique(i,j)=0;
                CG.Clique(j,i)=0;
            else
                CG.Clique(i,j)=0;
                CG.Clique(j,i)=0; 
            end
        end
    end
end
clear r i j Comp CompClique pos minSep d index