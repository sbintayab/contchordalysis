%% Computing the MML for encoding parameters and data for an edge (fn,sn)
function MML = MMLpd(edge,Data)
%% separating the nodes from the edge variable
fn=edge.fn;
sn=edge.sn;

%% Initializing the MML score
MML=0;

%% Preparing S
if sum(edge.MS) == 0
    S=-1;
else
    S=find(ismember(edge.MS,1));
end

%% the number of samples n and the genes d
[n,d] = size(Data);

%% Preparing S, SUa, SUb and SUab
if S ~= -1
    X=sort([fn S]);     % SUa
    Y=sort([S sn]);     % SUb
    Z=sort([fn S sn]);  % SUab
else
    X=fn;
    Y=sn;
    Z=sort([fn sn]);
end

%% Mean calculation
muX=mean(Data(:,X));
muY=mean(Data(:,Y));
muZ=mean(Data(:,Z));
if S ~= -1
    muS=mean(Data(:,S));
else
    muS=0;
end

%% Covariance matrix calculation
covX=cov(Data(:,X));
covY=cov(Data(:,Y));
covZ=cov(Data(:,Z));
if S ~= -1
    covS=cov(Data(:,S));
else
    covS=0;
end

%% MML calculation for X
MMLX=0;
dcovX=det(covX);
logp = sum(logmvnpdf(Data(:,X),muX,covX));
d=size(X,2); % dimention of X
MMLX = (((n-d-3)/2)*log(dcovX))+logp;
clear d X dcovX muX logp covX

%% MML calculation for Y
MMLY=0;
dcovY=det(covY);
logp = sum(logmvnpdf(Data(:,Y),muY,covY));
d=size(Y,2); % Dimention of Y
MMLY = (((n-d-3)/2)*log(dcovY))+logp;
clear d Y dcovY muY logp covY

%% MML calculation for Z
MMLZ=0;
dcovZ=det(covZ);
logp = sum(logmvnpdf(Data(:,Z),muZ,covZ));
d=size(Z,2); 
MMLZ = (((n-d-3)/2)*log(dcovZ))+logp;
clear d Z dcovZ muZ logp covZ

%% MML calculation for S
MMLS=0;
if S ~= -1
    dcovS=det(covS);
    logp = sum(logmvnpdf(Data(:,S),muS,covS));
    d=size(S,2);
    MMLS = (((n-d-3)/2)*log(dcovS))+logp;
    clear d dcovS logp
end
clear S muS covS

%% Computing the final MML for encoding data and parameter of an edge (fn,sn)
MML=-abs(MMLZ+MMLS-MMLX-MMLY);
clear MMLZ MMLS MMLX MMLY